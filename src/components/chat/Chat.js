import React, { Component } from 'react'
import { connect } from 'react-redux'
import EmojiPicker from 'emoji-picker-react'
import { closest } from '../../helpers/helpers'
import Message from './message/Message'
import { loadMessages, incrementUnreadedMessages } from '../app/AppActions'
import './Chat.scss'

class Chat extends Component {
    constructor(props) {
        super(props)

        this.state = {
            input: '',
            emojiShow: false
        }

        // Emoji picker flag
        this.emojiShown = false
    }

    /**
     * Handler for keyUp
     */
    onKeyUp = (e) => {
        // Send message on enter
        if (e.keyCode === 13 && this.props.sendOnEnter === 'on') {
            e.preventDefault()
            this.sendMessage()
        }
    }

    /**
     * Control input value through state
     * @param { Object }    e   Input change event
     */
    setInputValue = (e) => {
        this.setState({
            input: e.target.value
        })
    }

    /**
     * Send message from input to the server
     * @param { Object }    e   Form submit event
     */
    sendMessage = (e) => {
        e && e.preventDefault()

        if (this.state.input) {
            this.props.socket.emit('message', {
                message: this.state.input,
                user: this.props.user
            })

            this.setState({
                input: ''
            })

            // Focus input
            this.input.focus()
        }
    }

    /**
     * Scroll messages container to the bottom
     */
    scrollToBottom = () => {
        this.DOM.chatContainer.scrollTop = this.DOM.chatContainer.scrollHeight
    }

    /**
     * Show emoji picker
     */
    showEmojiPicker = () => {
        this.setState({ emojiShow: true })
        setTimeout(() => {
            this.emojiShown = true
        })
    }

    /**
     * Emoji click callback
     */
    handleEmojiClick = (code, emoji) => {
        this.setState({
            input: this.state.input + ` :${emoji.name}: `,
            emojiShow: false
        })
        this.emojiShown = false

        this.input.focus()
    }

    componentDidMount() {
        this.DOM = {
            chatContainer: document.getElementById('chatContainer')
        }

        this.scrollToBottom()

        // Reset unreaded messages
        this.props.incrementUnreadedMessages(0)

        // Close emoji picker
        document.addEventListener('click', (e) => {
            if (this.emojiShown && !closest(e.target, '.js-emoji-picker-wrapper')) {
                this.setState({ emojiShow: false })
                this.emojiShown = false
            }
        })
    }

    componentDidUpdate() {
        this.scrollToBottom()
    }

    render() {
        return (
            <section className="p-chat p-page-body">
                <div className="page-container container" id="chatContainer">
                    {this.props.messages.map((m) => {
                        return <Message message={m} user={this.props.user} clock={this.props.clock} key={m.id} />
                    })}
                </div>
                <form className="chat-form" onSubmit={this.sendMessage}>
                    <textarea
                        type="text"
                        className="c-input message-input"
                        placeholder={this.props.localization.typeMessage}
                        value={this.state.input}
                        ref={(c) => { this.input = c }}
                        onKeyUp={this.onKeyUp}
                        onChange={this.setInputValue} />
                    <button type="button" className="emoji-pick" onClick={this.showEmojiPicker}>
                        <span className="emoji-pick-icon icon-tongue"></span>
                    </button>
                    <div className="button-wrapper">
                        <button className="send-button"><i className="icon-paper-plane icon"></i></button>
                    </div>
                </form>
                <div className={`${this.state.emojiShow ? '' : 'hide-emoji'} js-emoji-picker-wrapper`}>
                    <EmojiPicker emojiResolution={64} onEmojiClick={this.handleEmojiClick} />
                </div>
            </section>
        )
    }
}

function mapStateToProps (state) {
    return {
        socket: state.socket,
        user: state.user,
        messages: state.messages
    }
}

export default connect(mapStateToProps, {
    loadMessages,
    incrementUnreadedMessages
})(Chat)
