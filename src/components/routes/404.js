import React from 'react'
import { Link } from 'react-router-dom'

export default () => {
    return (
        <div className="pg-404">
            <h1>Bad luck...</h1>
            <Link to="/">Go to home page</Link>
        </div>
    )
}
