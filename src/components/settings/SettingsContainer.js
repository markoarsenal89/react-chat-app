import React, { Component } from 'react'
import { connect } from 'react-redux'
import { debounce } from 'lodash'
import { loadUser, incrementUnreadedMessages } from '../app/AppActions'
import { saveUser } from '../../helpers/helpers'
import Settings from './Settings'

class SettingsContainer extends Component {
    /**
     * Update app settings
     * @param { Object }    e          Event Event from input or value from React Select plugin
     * @param { String }    setting    Type of a setting
     */
    updateSettings = (e, setting) => {
        this.props.updateAppSettings({
            ...this.props.settings,
            [setting]: e.target ? e.target.value : e // Second case is for React Select plugin
        })
    }

    /**
     * Update username
     * @param { Object }    username   New username
     */
    updateUsername = (username) => {
        const newUser = {...this.props.user, username }

        this.props.loadUser(newUser)
        saveUser(newUser)

        this.updateUsernameOnServer()
    }

    /**
     * Update username for all messages
     */
    updateUsernameOnServer = debounce(() => {
        this.props.socket.emit('user-update', this.props.user)
    }, 1000)

    /**
     * Increment unreaded messages counter
     */
    incrementUnreadedMessages = () => {
        this.props.incrementUnreadedMessages(this.props.unreadedMessages + 1)
    }

    componentDidMount() {
        // Messages
        this.props.socket.on('message', this.incrementUnreadedMessages)
    }

    componentWillUnmount() {
        this.props.socket.off('message', this.incrementUnreadedMessages)
    }

    render() {
        const p = this.props

        return <Settings
            {...p.settings}
            user={this.props.user}
            localization={p.localization[p.settings.language]}
            updateSettings={this.updateSettings}
            resetSettings={p.resetAppSettings}
            updateUsername={this.updateUsername} />
    }
}

function mapStateToProps (state) {
    return {
        socket: state.socket,
        user: state.user,
        unreadedMessages: state.unreadedMessages
    }
}

export default connect(mapStateToProps, {
    loadUser,
    incrementUnreadedMessages
})(SettingsContainer)
