import React, { Component } from 'react'
import { AppContext } from '../app/App'
import Header from '../header/Header'
import SettingsContainer from '../settings/SettingsContainer'

export default class SettingsPage extends Component {
    render() {
        return (
            <AppContext.Consumer>
                {(context) => {
                    return (
                        <div className="pg-settings pg-wrapper">
                            <Header />
                            <SettingsContainer {...context} />
                        </div>
                    )
                }}
            </AppContext.Consumer>
        )
    }
}
