/**
 * Get settings from localStorage
 */
export const getSettings = () => {
    return JSON.parse(window.localStorage.getItem('chatAppSettings'))
}

/**
 * Save settings into localStorage
 * @param { Object } settings
 */
export const saveSettings = (settings) => {
    window.localStorage.setItem('chatAppSettings', JSON.stringify(settings))
}

/**
 * Get user from localStorage
 */
export const getUser = () => {
    return JSON.parse(window.localStorage.getItem('chatAppUser'))
}

/**
 * Save user to localStorage
 */
export const saveUser = (user) => {
    window.localStorage.setItem('chatAppUser', JSON.stringify(user))
}

/**
 *
 * @param { HTML node }     el
 * @param { String }        selector
 */
export const closest = (el, selector) => {
    let matchesFn

    // find vendor prefix
    ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
        if (typeof document.body[fn] === 'function') {
            matchesFn = fn
            return true
        }

        return false
    })

    let parent

    // Traverse parents
    while (el) {
        parent = el.parentElement

        if (parent && parent[matchesFn](selector)) {
            return parent
        }

        el = parent
    }

    return null
}