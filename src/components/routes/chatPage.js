import React, { Component } from 'react'
import { AppContext } from '../app/App'
import Header from '../header/Header'
import Chat from '../chat/Chat'

export default class ChatPage extends Component {
    render() {
        return (
            <AppContext.Consumer>
                {(context) => {
                    const s = context.settings
                    const l = context.localization[s.language]

                    return (
                        <div className="pg-chat pg-wrapper">
                            <Header />
                            <Chat
                                localization={l}
                                clock={s.clock}
                                sendOnEnter={s.sendOnEnter} />
                        </div>
                    )
                }}
            </AppContext.Consumer>
        )
    }
}
