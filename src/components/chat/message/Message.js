import React from 'react'
import moment from 'moment'
import JsEmoji from 'emoji-js'
import './Message.scss'

export default (props) => {
    const jsEmoji = new JsEmoji()
    // Set the style to emojione (default - apple)
    jsEmoji.img_set = 'emojione'
    // Set the storage location for all emojis
    jsEmoji.img_sets.emojione.path = 'https://cdn.jsdelivr.net/emojione/assets/3.0/png/64/'
    jsEmoji.allow_native = false;

    const formatDate = (date) => {
        const d = moment(date)
        const today = d.clone().startOf('day')
        const format = props.clock === 'hours12' ? 'hh:mm A' : 'HH:mm'

        if (d.isSame(today, 'd')) {
            return d.format(format)
        } else {
            return d.format(`DD.MM. ${format}`)
        }
    }

    const emojiRegex = /:[a-zA-Z0-9_-]*:/ig
    const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|])/ig
    const ytRegex = /(?:youtube\.com\/\S*(?:(?:\/e(?:mbed))?\/|watch\/?\?(?:\S*?&?v=))|youtu\.be\/)([a-zA-Z0-9_-]{6,11})/ig;
    const imgRegex = /(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|jpeg|gif|png)/ig

    /**
     * Render message with links, Youtube embed and images
     */
    const renderMessage = () => {
        let message = props.message.message

        const matches = message.match(urlRegex) || []

        // Format links, Youtube and images
        matches.forEach((match) => {
            // Check for Youtube
            if (ytRegex.test(match)) {
                message = generateYTEmbed(message, match)
                return
            }

            // Check for image
            if (imgRegex.test(match)) {
                message = generateImg(message, match)
                return
            }

            // Other links
            message = message.replace(match, `<a href="${match}" target="_blank">${match}</a>`)
        })

        // Set emojis for the message
        message = setEmojis(message)

        return message
    }

    const setEmojis = (message) => {
        const matches = message.match(emojiRegex) || []

        matches.forEach((match) => {
            const emojiImg = jsEmoji.replace_colons(match)
            message = message.replace(match, emojiImg)
        })

        return message
    }

    /**
     * Generate iframe for Youtube video
     * @param { String }    message     User message
     * @param { String }    match       Matched url in the message
     */
    const generateYTEmbed = (message, match) => {
        const embed = `<div class="video-embed">
                            <iframe src="https://www.youtube.com/embed/${getYTId(match)}"
                                frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>`

        message = message.replace(match, embed)

        return message
    }

    /**
     * Get Youtube video id from url
     * @param { String }    url     Youtube url
     */
    const getYTId = (url) => {
        let id = ''

        url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/)

        if (url[2] !== undefined) {
            id = url[2].split(/[^0-9a-z_-]/i)
            id = id[0]
        } else {
            id = url
        }

        return id
    }

    /**
     * Generate img tag
     * @param { String }    message     User message
     * @param { String }    match       Matched url in the message
     */
    const generateImg = (message, match) => {
        const img = `<div class="img-embed">
                        <img src="${match}" />
                    </div>`

        message = message.replace(match, img)

        return message
    }

    const isCurrentUser = props.message.user.id === props.user.id
    const messageType = isCurrentUser ? 'current-user' : ''
    const date = formatDate(props.message.date)
    const headerContent = isCurrentUser ? date : `${props.message.user.username || 'no name'} | ${date}`

    return (
        <div className={`c-message ${messageType}`}>
            <div className="message-unit">
                <header className="message-header">{headerContent}</header>
                <div className="message-body">
                    <div className="user"><div className="user-avatar"></div></div>
                    <div className="message-content">
                        <div className="message-text" dangerouslySetInnerHTML={{ __html: renderMessage() }}></div>
                    </div>
                </div>
            </div>
        </div>
    )
}