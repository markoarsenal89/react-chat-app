import React, { Component } from 'react'
import { connect } from 'react-redux'
import { AppContext } from '../app/App'
import { NavLink } from 'react-router-dom'
import './Header.scss'

class Header extends Component {
    /**
     * Render number of unreaded messages
     */
    renderUnreadedMessages = () => {
        if (this.props.unreadedMessages > 0) {
            return (
                <div className="unreaded-messages">{this.props.unreadedMessages}</div>
            )
        }
    }

    render() {
        return (
            <AppContext.Consumer>
                {(context) => {
                    const l = context.localization[context.settings.language]

                    return (
                        <header className="l-header container">
                            <NavLink exact={true} activeClassName='active' className="header-tab" to="/">
                                {this.renderUnreadedMessages()}
                                {l.chat}
                            </NavLink>
                            <NavLink activeClassName='active' className="header-tab" to="/settings">{l.settings}</NavLink>
                        </header>
                    )
                }}
            </AppContext.Consumer>
        )
    }
}

function mapStateToProps (state) {
    return { unreadedMessages: state.unreadedMessages }
}

export default connect(mapStateToProps)(Header)
