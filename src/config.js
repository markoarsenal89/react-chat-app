export const host = 'http://localhost'
export const port = '8000'
export const localization = {
    english: {
        username: 'Username',
        enterUsername: 'Enter username...',
        interfaceColor: 'Interface color',
        light: 'Light',
        dark: 'Dark',
        clockDisplay: 'Clock display',
        hours12: '12 hours',
        hours24: '24 hours',
        sendOnEnter: 'Send message on ENTER',
        on: 'On',
        off: 'Off',
        language: 'Language',
        english: 'English',
        serbian: 'Serbian',
        resetToDefaults: 'Reset to defaults',
        chat: 'chat',
        settings: 'settings',
        typeMessage: 'Type your message...'
    },
    serbian: {
        username: 'Korisničko ime',
        enterUsername: 'Unesite korisničko ime...',
        interfaceColor: 'Boja interfejsa',
        light: 'Svetla',
        dark: 'Tamna',
        clockDisplay: 'Prikaz vremena',
        hours12: '12 sati',
        hours24: '24 sata',
        sendOnEnter: 'Pošalji poruku na ENTER',
        on: 'Uključi',
        off: 'Isključi',
        language: 'Jezik',
        english: 'Engleski',
        serbian: 'Srpski',
        resetToDefaults: 'Resetuj podešavanja',
        chat: 'čet',
        settings: 'podešavanja',
        typeMessage: 'Unesite poruku...'
    }
}