export const SAVE_SOCKET = 'SAVE_SOCKET'
export const LOAD_USER = 'LOAD_USER'
export const LOAD_MESSAGES = 'LOAD_MESSAGES'
export const UNREADED_MESSAGES = 'UNREADED_MESSAGES'

export const saveSocket = (socket) => {
    return {
        type: SAVE_SOCKET,
        socket
    }
}

export const loadUser = (user) => {
    return {
        type: LOAD_USER,
        user
    }
}

export const loadMessages = (messages) => {
    return {
        type: LOAD_MESSAGES,
        messages
    }
}

export const incrementUnreadedMessages = (unreadedMessages) => {
    return {
        type: UNREADED_MESSAGES,
        unreadedMessages
    }
}
