## React chat app

### How to setup
* Clone this repo
* Run npm install

### How to run
* Start the server with: `node src/server.js`
* Run `npm start` in another terminal

## Enjoy chatting... :)
