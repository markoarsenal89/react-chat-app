import React, { Component } from 'react'
import io from 'socket.io-client'
import { host, port, localization } from '../../config'
import { connect } from 'react-redux'
import { saveSocket, loadUser, loadMessages } from './AppActions'
import { saveSettings, getSettings, getUser, saveUser } from '../../helpers/helpers'
import Routes from '../routes/'
import './App.scss'

export const AppContext = React.createContext()

class App extends Component {
    constructor(props) {
        super(props)

        this.settings = getSettings()
        this.defaultSettings = {
            theme: 'light',
            clock: 'hours12',
            sendOnEnter: 'on',
            language: 'english'
        }

        this.state = {
            localization: localization,
            settings: this.settings || this.defaultSettings
        }

        this.connectChat()
    }

    componentDidMount() {
        // DOM reference
        this.DOM = {
            html: document.getElementsByTagName('html')[0]
        }

        // Add theme class
        this.DOM.html.classList.add(`t-theme-${this.state.settings.theme}`)
    }

    componentWillUnmount() {
        // Remove sockets listeners
        this.props.socket.off('message')
        this.props.socket.off('load-messages')
    }

    /**
     * Connect to the server and register user
     */
    connectChat() {
        const socket = io.connect(`${host}:${port}`)
        let user = getUser()

        this.props.saveSocket(socket)

        // Create user
        socket.on('register-user', (user) => {
            saveUser(user)
            this.props.loadUser(user)
        })

        // Register user
        socket.emit('register-user', user || { id: -1, username: null })

        // Load initial messages
        socket.on('load-messages', (messages) => {
            this.props.loadMessages(messages)
        })

        // Listen for a message
        socket.on('message', (message) => {
            this.props.loadMessages([...this.props.messages, message])
        })
    }

    /**
     * Update app settings
     * @param { Object }    newSettings     New settings object
     */
    updateAppSettings = (newSettings) => {
        // Update theme class
        this.DOM.html.classList.remove(`t-theme-${this.state.settings.theme}`)
        this.DOM.html.classList.add(`t-theme-${newSettings.theme}`)

        this.setState({ settings: newSettings })
        saveSettings(newSettings)
    }

    /**
     * Reset app settings to default
     */
    resetAppSettings = () => {
        this.updateAppSettings(this.defaultSettings)
    }

    render() {
        return (
            <AppContext.Provider value={{
                ...this.state,
                updateAppSettings: this.updateAppSettings,
                resetAppSettings: this.resetAppSettings
            }}>
                <div className="app">
                    <Routes />
                </div>
            </AppContext.Provider>
        )
    }
}

function mapStateToProps (state) {
    return { messages: state.messages }
}

export default connect(mapStateToProps, {
    saveSocket,
    loadUser,
    loadMessages
})(App)
