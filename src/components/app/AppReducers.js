import { SAVE_SOCKET, LOAD_MESSAGES, LOAD_USER, UNREADED_MESSAGES } from './AppActions'

export function socket (state = null, action) {
    switch (action.type) {
        case SAVE_SOCKET:
            return action.socket
        default:
            return state
    }
}

export function user (state = { id: -1 }, action) {
    switch (action.type) {
        case LOAD_USER:
            return action.user
        default:
            return state
    }
}

export function messages (state = [], action) {
    switch (action.type) {
        case LOAD_MESSAGES:
            return action.messages
        default:
            return state
    }
}

export function unreadedMessages (state = 0, action) {
    switch (action.type) {
        case UNREADED_MESSAGES:
            return action.unreadedMessages
        default:
            return state
    }
}
