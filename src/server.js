const config = require('./configServer')
const io = require('socket.io')
const server = io.listen(config.port)

console.log(`Server listening on port ${config.port}...`)

const users = []
const messages = []

// Event fired every time a new client connects:
server.on('connection', (socket) => {
    console.log(`Client connected: ${socket.id}`);


    // When socket disconnects, remove it from the list
    socket.on('disconnect', () => {
        console.info(`Client gone: ${socket.id}`)
    })

    // Register a user
    socket.on('register-user', (user) => {
        let id = user.id
        let username = user.username

        if (!user.username) {
            username = generateGuestUsername()
        } else if (doesUsernameExists(user)) {
            username = generateGuestUsername()
        }

        if (user.id === -1) {
            id = socket.id
        }

        users.push({
            id,
            username,
            avatar: ''
        })

        socket.emit('register-user', {
            id,
            username
        })
    })

    // Load initial messages
    server.sockets.emit('load-messages', messages)

    // Message from client send back to all
    socket.on('message', (req) => {
        const m = {
            ...req,
            date: new Date(),
            id: messages.length
        }

        messages.push(m)
        server.sockets.emit('message', m)
    })

    // Change user info
    socket.on('user-update', (newUserInfo) => {
        users.forEach((u) => {
            if (u.id === newUserInfo.id && doesUsernameExists(newUserInfo)) {
                u.username = newUserInfo.username
            }
        })

        // Update messages
        messages.forEach((m) => {
            if (m.user.id === newUserInfo.id) {
                m.user.username = newUserInfo.username
            }
        })

        server.sockets.emit('load-messages', messages)
    })
})

const generateGuestUsername = () => {
    const animal = config.animals[getRandomInt(0, config.animals.length - 1)]

    return `${animal}-${getRandomInt(0, 1000)}`
}

const doesUsernameExists = (user) => {
    const foundedUser = users.find((u) => {
        return u.username === user.username && u.id !== user.id
    })

    return foundedUser ? true : false
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}
