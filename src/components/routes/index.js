import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import ChatPage from './chatPage'
import SettingsPage from './settingsPage'
import Page404 from './404'

export default () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={ChatPage}></Route>
                <Route path="/settings" component={SettingsPage}></Route>
                <Route component={Page404}></Route>
            </Switch>
        </BrowserRouter>
    )
}
