import { combineReducers } from 'redux'
import { socket, user, messages, unreadedMessages } from './components/app/AppReducers'

export default combineReducers({
    socket,
    user,
    messages,
    unreadedMessages
})