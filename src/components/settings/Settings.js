import React from 'react'
import Select from 'react-select'
import 'react-select/dist/react-select.css'

export default (props) => {
    return (
        <section className="p-settings p-page-body">
            <div className="container page-container">
                <div className="c-form-group">
                    <label className="c-label group-label" htmlFor="username">{props.localization.username}</label>
                    <input
                        className="c-input"
                        type="text"
                        placeholder={props.localization.enterUsername}
                        onChange={(e) => {
                            props.updateUsername(e.target.value)
                        }}
                        value={props.user.username || ''}
                        id="username" />
                </div>
                <div className="c-form-group">
                    <label className="c-label group-label">{props.localization.interfaceColor}</label>
                    <div className="c-form-inline-group">
                        <div className="c-radio">
                            <input
                                type="radio"
                                name="interface"
                                id="light"
                                value="light"
                                onChange={(e) => { props.updateSettings(e, 'theme') }}
                                checked={props.theme === 'light' ? true : false} />
                            <label htmlFor="light" className="radio-label">{props.localization.light}</label>
                        </div>
                    </div>
                    <div className="c-form-inline-group">
                        <div className="c-radio">
                            <input
                                type="radio"
                                name="interface"
                                id="dark"
                                value="dark"
                                onChange={(e) => { props.updateSettings(e, 'theme') }}
                                checked={props.theme === 'dark' ? true : false} />
                            <label htmlFor="dark" className="radio-label">{props.localization.dark}</label>
                        </div>
                    </div>
                </div>
                <div className="c-form-group">
                    <label className="c-label group-label">{props.localization.clockDisplay}</label>
                    <div className="c-form-inline-group">
                        <div className="c-radio">
                            <input
                                type="radio"
                                name="clock"
                                id="hours12"
                                value="hours12"
                                onChange={(e) => { props.updateSettings(e, 'clock') }}
                                checked={props.clock === 'hours12' ? true : false} />
                            <label htmlFor="hours12" className="radio-label">{props.localization.hours12}</label>
                        </div>
                    </div>
                    <div className="c-form-inline-group">
                        <div className="c-radio">
                            <input
                                type="radio"
                                name="clock"
                                id="hours24"
                                value="hours24"
                                onChange={(e) => { props.updateSettings(e, 'clock') }}
                                checked={props.clock === 'hours24' ? true : false} />
                            <label htmlFor="hours24" className="radio-label">{props.localization.hours24}</label>
                        </div>
                    </div>
                </div>
                <div className="c-form-group">
                    <label className="c-label group-label">{props.localization.sendOnEnter}</label>
                    <div className="c-form-inline-group">
                        <div className="c-radio">
                            <input
                                type="radio"
                                name="sendOnEnter"
                                id="sendOnEnterOn"
                                value="on"
                                onChange={(e) => { props.updateSettings(e, 'sendOnEnter') }}
                                checked={props.sendOnEnter === 'on' ? true : false} />
                            <label htmlFor="sendOnEnterOn" className="radio-label">{props.localization.on}</label>
                        </div>
                    </div>
                    <div className="c-form-inline-group">
                        <div className="c-radio">
                            <input
                                type="radio"
                                name="sendOnEnter"
                                id="sendOnEnterOff"
                                value="off"
                                onChange={(e) => { props.updateSettings(e, 'sendOnEnter') }}
                                checked={props.sendOnEnter === 'off' ? true : false} />
                            <label htmlFor="sendOnEnterOff" className="radio-label">{props.localization.off}</label>
                        </div>
                    </div>
                </div>
                <div className="c-form-group">
                    <label className="c-label group-label">{props.localization.language}</label>
                    <Select
                        value={props.language}
                        onChange={(selectedOption) => { props.updateSettings(selectedOption.value, 'language') }}
                        options={[
                            {
                                value: 'english',
                                label: props.localization.english
                            }, {
                                value: 'serbian',
                                label: props.localization.serbian
                            }
                        ]}
                        searchable={false}
                        clearable={false} />
                </div>
                <div className="c-form-group">
                    <button className="c-btn btn-block btn-light" onClick={props.resetSettings}>
                        <span className="icon icon-format_color_reset"></span>
                        {props.localization.resetToDefaults}
                    </button>
                </div>
            </div>
        </section>
    )
}